# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 0.2a
### Fixed
- Case for ZbStatus1 vs ZBStatus1

## 0.2
### Fixed
- Wrap requests in try/except

## 0.1.11
### Added
- We now publish $LAST_SEEN topic with the last seen epoch of a device

## 0.1.10
### Added
- Send uptime to the MQTT server for monitoring

### Changed
- Redid the time convertions

## 0.1.9.1
### Changed
- Reverted the "Name" block of 0.1.9 since it caused the addon to hang

## 0.1.9
### Added
- Support to define the sensor type for dimmers - they can be connected to Fans,lights etc (follow the HA docs)
### Changed
- Device names are now used to define the device type. The name is split into 2, using :
  The first section is used as the name, the second section is used as the device type

## 0.1.8.1
### Added
- Support for Aqara Water Sensors (might work for others as well)

## 0.1.7
### Added
- Included a check incase we got a message for a device we don't know it (early stages of adding a device)
### Fixed
- 2 Python errors for Python 3.8+

## 0.1.6
### Added
- Added support for Sonoff WB01
- We now publish a $DEV_INFO topic with what we know of a device in MQTT

## 0.1.5
### Added
- We now pass Pressure and SeaPressure to MQTT

## 0.1.4
### Changed
- We no longer *need* the Name field in the payload, if it'snot present we replace it with Device ID
- Moved the exception handling to a better place
### Added
- Ability to now add smoke detectors

## 0.1.3
### Changed
- MQTT messages to HA is now retained. This should help with devices staying after a HA reboot/restart

## 0.1.2
### Added
- Ability to use a single slider for multiple dimmers (set the topic to "A" for the Endpoint)

## 0.1.1a
### Added
- Remove requirement for the Tasmota Topic - tnz2ha will now get it from the device

### Fixed
- Occupancy sensor was broken due to missing Endpoint

## 0.1.1
### Added
- Dimmer (with switch) support
- We now have command topics so that HA can "talk back"

## 0.1.0
- First commit
