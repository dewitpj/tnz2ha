#!/usr/bin/with-contenv bashio

CONFIG_PATH=/data/options.json

python3 /run.py --mqtt-host "$(bashio::config 'mqtt_host')" --mqtt-port "$(bashio::config 'mqtt_port')" --mqtt-user "$(bashio::config 'mqtt_user')" --mqtt-pass "$(bashio::config 'mqtt_pass')" --tasm-host "$(bashio::config 'tasm_host')" --tasm-port "$(bashio::config 'tasm_port')" --tasm-user "$(bashio::config 'tasm_user')" --tasm-pass "$(bashio::config 'tasm_pass')" --mqtt-topic "$(bashio::config 'mqtt_topic')" --mqtt-disc "$(bashio::config 'mqtt_disc')"
