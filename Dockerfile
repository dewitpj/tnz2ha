ARG BUILD_FROM
FROM $BUILD_FROM

ENV LANG C.UTF-8

# Install python and the needed libs
RUN apk add --no-cache python3
RUN apk add --no-cache py3-paho-mqtt
RUN apk add --no-cache py3-requests

WORKDIR /

# Copy data for add-on
COPY run.sh /
COPY run.py /
RUN chmod a+x /run.sh

CMD [ "/run.sh" ]
