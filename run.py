from datetime import datetime
import paho.mqtt.client as mqtt
import time
import os
import sys
import json
import argparse
import requests
import datetime
import logging

run=True

mqtt_disc="homeassistant"

devices={}
client=mqtt.Client()

version="0.2f"

logger=logging.getLogger ()
logger.setLevel (logging.INFO)

def process_misc (client,payload):
    global devices
    #global client

    client.publish ("tnz2ha/payload",str (payload),0,True)
    client.publish ("tnz2ha/payload_type",str (type (payload)),0,True)

    try:
        client.publish ("tnz2ha/payload_item",str (payload [0]),0,True)
        client.publish ("tnz2ha/payload_item_1",str (payload [1]),0,True)
    except:
        client.publish ("tnz2ha/payload_item","no",0,True)

    # We are defaulting to "lights" for backwards compatibility
    devtype="light"

    if ("Device" in payload):
        if (payload ["Device"] not in devices):
            client.publish ("tnz2ha/device_not_found",payload ["Device"],0,True)
            return None;
    else:
        client.publish ("tnz2ha/device_not_in_payload",str (payload))
        logging.error ("\"Device\" not in payload")
        return None;
    # General Stuff - seen on all sensors so far
    if ("Endpoint" in payload):
        dev_id=str (devices [payload ["Device"]]["IEEEAddr"])+"_"+str (payload ["Endpoint"])
    else:
        dev_id=str (devices [payload ["Device"]]["IEEEAddr"])+"_1"
    if ("Name" in payload):
        if (":" in payload ["Name"]):
            name_split=payload ["Name"].split (":")
 #           print (name_split)
            name=name_split [0]
            devtype=name_split [1] 
        else:
            name=payload ["Name"]
#    if ("Name" in payload):
#        name=payload ["Name"]
    else:
        name="Not Named "+str (payload ["Device"])

    # Publish what we know about the device to a topic
    client.publish ("tnz2ha/"+dev_id+"/$DEV_INFO",json.dumps (devices [payload ["Device"]]),0,True)
    client.publish ("tnz2ha/"+dev_id+"/$LAST_SEEN",str (time.time ()),0,True)
    if ("BatteryPercentage" in payload):
        client.publish (mqtt_disc+"/sensor/"+dev_id+"/BatteryPercentage/config","{\"name\":\""+name+" Battery Percentage\",\"unique_id\":\""+dev_id+"_battery_percent\",\"device_class\":\"battery\",\"unit_of_measurement\": \"%\", \"state_topic\":\"tnz2ha/"+dev_id+"/battery_percent\"}",0,True)
        client.publish ("tnz2ha/"+dev_id+"/battery_percent",str (payload ["BatteryPercentage"]),0,True)
    if ("BatteryVoltage" in payload):
        client.publish (mqtt_disc+"/sensor/"+dev_id+"/BatteryVoltage/config","{\"name\":\""+name+" Battery Voltage\",\"unique_id\":\""+dev_id+"_battery_voltage\",\"device_class\":\"voltage\",\"unit_of_measurement\": \"V\", \"state_topic\":\"tnz2ha/"+dev_id+"/battery_voltage\"}",0,True)
        client.publish ("tnz2ha/"+dev_id+"/battery_voltage",payload ["BatteryVoltage"],0,True)
    if ("LinkQuality" in payload):
        client.publish (mqtt_disc+"/sensor/"+dev_id+"/LinkQuality/config","{\"name\":\""+name+" Link Quality\",\"unique_id\":\""+dev_id+"_link_quality\",\"device_class\":\"signal_strength\",\"unit_of_measurement\": \"dBm\", \"state_topic\":\"tnz2ha/"+dev_id+"/linkqual\"}",0,True)
        client.publish ("tnz2ha/"+dev_id+"/linkqual",str (payload ["LinkQuality"]),0,True)
    if ("ModelId" in payload):
        client.publish ("tnz2ha/"+dev_id+"/modelid",payload ["ModelId"],0,True)
    # Temperature and Humidity
    if ("Temperature" in payload):
        client.publish (mqtt_disc+"/sensor/"+dev_id+"/Temperature/config","{\"name\":\""+name+" Temperature\",\"unique_id\":\""+dev_id+"_temprature\",\"device_class\":\"temperature\",\"unit_of_measurement\": \"°C\", \"state_topic\":\"tnz2ha/"+dev_id+"/temperature\"}",0,True)
        client.publish ("tnz2ha/"+dev_id+"/temperature",payload ["Temperature"],0,True)
    if ("Humidity" in payload):
        client.publish (mqtt_disc+"/sensor/"+dev_id+"/Humidity/config","{\"name\":\""+name+" Humidity\",\"unique_id\":\""+dev_id+"_humidity\",\"device_class\":\"humidity\",\"unit_of_measurement\": \"%\", \"state_topic\":\"tnz2ha/"+dev_id+"/humidity\"}",0,True)
        client.publish ("tnz2ha/"+dev_id+"/humidity",payload ["Humidity"],0,True)
    if ("Pressure" in payload):
        client.publish (mqtt_disc+"/sensor/"+dev_id+"/Pressure/config","{\"name\":\""+name+" Pressure\",\"unique_id\":\""+dev_id+"_pressure\",\"device_class\":\"pressure\",\"unit_of_measurement\": \"hPa\",\"state_topic\":\"tnz2ha/"+dev_id+"/pressure\"}",0,True)
        client.publish ("tnz2ha/"+dev_id+"/pressure",payload ["Pressure"],0,True)
    if ("SeaPressure" in payload):
        client.publish (mqtt_disc+"/sensor/"+dev_id+"/SeaPressure/config","{\"name\":\""+name+" Sea Pressure\",\"unique_id\":\""+dev_id+"_seapressure\",\"device_class\":\"pressure\",\"unit_of_measurement\": \"hPa\",\"state_topic\":\"tnz2ha/"+dev_id+"/seapressure\"}",0,True)
        client.publish ("tnz2ha/"+dev_id+"/seapressure",payload ["SeaPressure"],0,True)
    # Button
    if ("Click" in payload):
        client.publish (mqtt_disc+"/button/"+dev_id+"/1/state","{\"state\":\""+payload ["Click"]+"\"}",0,True)
        client.publish ("tnz2ha/"+dev_id+"/state",payload ["Click"],0,False)
    # Motion Sensor
    if ("Occupancy" in payload):
        client.publish (mqtt_disc+"/binary_sensor/"+dev_id+"/"+dev_id+"/config","{\"name\":\""+name+"\",\"unique_id\":\""+dev_id+"_motion\",\"device_class\": \"motion\", \"state_topic\": \"tnz2ha/"+dev_id+"/state\"}",0,True)
        if (payload ["Occupancy"]==1):
           client.publish ("tnz2ha/"+dev_id+"/state","ON",0,True)
        else:
           client.publish ("tnz2ha/"+dev_id+"/state","OFF",0,True)
    # Magnet Sensor
    if ("Contact" in payload):
        client.publish (mqtt_disc+"/binary_sensor/"+dev_id+"/"+dev_id+"/config","{\"name\":\""+name+"\",\"unique_id\":\""+dev_id+"_magnet\",\"device_class\": \"door\", \"state_topic\": \"tnz2ha/"+dev_id+"/state\"}",0,True)
        if (payload ["Contact"]==1):
            client.publish ("tnz2ha/"+dev_id+"/state","ON",0,True)
        else:
            client.publish ("tnz2ha/"+dev_id+"/state","OFF",0,True)
    # Aqara Water Sensor
    if ("ZoneStatusChange" in payload):
        client.publish (mqtt_disc+"/binary_sensor/"+dev_id+"/"+dev_id+"/config","{\"name\":\""+name+"\",\"unique_id\":\""+dev_id+"_water\",\"device_class\": \"moisture\", \"state_topic\": \"tnz2ha/"+dev_id+"/state\"}",0,True)
        if (payload ["ZoneStatusChange"]==1):
            client.publish ("tnz2ha/"+dev_id+"/state","ON",0,True)
        else:
            client.publish ("tnz2ha/"+dev_id+"/state","OFF",0,True)
    # Switch
    if ("Power" in payload):
        if ("Dimmer" in devices [payload ["Device"]]):
            client.publish (mqtt_disc+"/"+devtype+"/"+dev_id+"/"+dev_id+"/config","{\"name\":\""+name+"\",\"unique_id\":\""+dev_id+"_dimmer\",\"state_topic\": \"tnz2ha/"+dev_id+"/state\", \"command_topic\": \"tnz2ha/cmnd/"+payload ["Device"]+"/"+str (payload ["Endpoint"])+"/state_cmd\", \"brightness_state_topic\": \"tnz2ha/"+dev_id+"/dimmer\", \"brightness_command_topic\": \"tnz2ha/cmnd/"+payload ["Device"]+"/"+str (payload ["Endpoint"])+"/dimmer_cmd\"}",0,True)
        else:
            if devices [payload ["Device"]]["ModelId"]=="WB01":
                # Special case for the Sonoff Buttons - they use "Power" to indicate what was done to the button
                if payload ["Power"]==2:
                    client.publish ("tnz2ha/"+dev_id+"/state","single",0,False)
                if payload ["Power"]==1:
                    client.publish ("tnz2ha/"+dev_id+"/state","double",0,False)
                if payload ["Power"]==0:
                    client.publish ("tnz2ha/"+dev_id+"/state","long",0,False)
            else:
                client.publish (mqtt_disc+"/switch/"+dev_id+"/"+dev_id+"/config","{\"name\":\""+name+"\",\"unique_id\":\""+dev_id+"_switch\", \"state_topic\": \"tnz2ha/"+dev_id+"/state\",\"command_topic\": \"tnz2ha/cmnd/"+payload ["Device"]+"/"+str (payload ["Endpoint"])+"/state_cmd\"}",0,True)
        # Don't override what we did above, if we did it
        if (not devices [payload ["Device"]]["ModelId"]=="WB01"):
            if (payload ["Power"]==1):
                client.publish ("tnz2ha/"+dev_id+"/state","ON",0,True)
            else:
                client.publish ("tnz2ha/"+dev_id+"/state","OFF",0,True)
    # Dimmer
    if ("Dimmer" in payload):
        client.publish (mqtt_disc+"/"+devtype+"/"+dev_id+"/"+dev_id+"/config","{\"name\":\""+name+"\",\"unique_id\":\""+dev_id+"_dimmer\",\"state_topic\": \"tnz2ha/"+dev_id+"/state\", \"command_topic\": \"tnz2ha/cmnd/"+payload ["Device"]+"/"+str (payload ["Endpoint"])+"/state_cmd\", \"brightness_state_topic\": \"tnz2ha/"+dev_id+"/dimmer\", \"brightness_command_topic\": \"tnz2ha/cmnd/"+payload ["Device"]+"/"+str (payload ["Endpoint"])+"/dimmer_cmd\"}",0,True)
        client.publish ("tnz2ha/"+dev_id+"/dimmer",payload ["Dimmer"],0,True)
    # Smoke Sensor
    if ("Fire" in payload):
        client.publish (mqtt_disc+"/binary_sensor/"+dev_id+"/"+dev_id+"/config","{\"name\":\""+name+"\",\"unique_id\":\""+dev_id+"_smoke\",\"device_class\": \"smoke\", \"state_topic\": \"tnz2ha/"+dev_id+"/state\"}",0,True)
        if (payload ["Fire"]==1):
           client.publish ("tnz2ha/"+dev_id+"/state","ON",0,True)
        else:
           client.publish ("tnz2ha/"+dev_id+"/state","OFF",0,True)

def on_connect (client, userdata, flags, rc):
    #global client

    client.subscribe (mqtt_topic)
    client.subscribe ("tnz2ha/cmnd/+/+/state_cmd")
    client.subscribe ("tnz2ha/cmnd/+/+/dimmer_cmd")
    client.subscribe ("tele/+/SENSOR")

def on_message (client, userdata, msg):
    global tasm_topic
    global devices
    #global client

    topic=str (msg.topic)
    payload_str=str (msg.payload.decode ("utf8"))
    client.publish ("tnz2ha/debug_topic",str (topic),0,True)
    client.publish ("tnz2ha/debug_msg",str (payload_str),0,True)
    #client.publish ("tnz2ha/debug_devices",json.dumps (devices),0,True)
    if (msg.topic.startswith ("tnz2ha")):
        if (tasm_topic!=""):
            topic_split=topic.split ("/")
            start=1
            count_to=1
            if (str (topic_split [3]) == "A"):
                start=1
                count_to=len (devices [topic_split [2]]["Endpoints"])
            else:
                start=int (topic_split [3])
                count_to=start

            for count in range (start,count_to+1):
                if (topic.endswith ("/state_cmd")):
                    client.publish ("cmnd/"+tasm_topic+"/ZbSend","{\"Device\":\""+topic_split [2]+"\",\"Endpoint\":\""+str (count)+"\",\"Send\":{\"Power\":\""+payload_str+"\"}}",0,False)
                if (topic.endswith ("/dimmer_cmd")):
                    dimmer_val=0
                    if (topic_split [3] == "A"):
                        dimmer_val=round (int (payload_str)*2.55)
                    else:
                        dimmer_val=int (payload_str)
                    if (dimmer_val>254):
                        dimmer_val=254
                    client.publish ("cmnd/"+tasm_topic+"/ZbSend","{\"Device\":\""+topic_split [2]+"\",\"Endpoint\":\""+str (count)+"\",\"Send\":{\"Dimmer\":\""+str (dimmer_val)+"\"}}",0,False)
                    if (dimmer_val==0):
                        client.publish ("cmnd/"+tasm_topic+"/ZbSend","{\"Device\":\""+topic_split [2]+"\",\"Endpoint\":\""+str (count)+"\",\"Send\":{\"Power\":\"OFF\"}}",0,False)
                    else:
                        client.publish ("cmnd/"+tasm_topic+"/ZbSend","{\"Device\":\""+topic_split [2]+"\",\"Endpoint\":\""+str (count)+"\",\"Send\":{\"Power\":\"ON\"}}",0,False)
    else:
        try:
            payload=json.loads (payload_str)
        except Exception as e:
            client.publish ("tnz2ha/debug_error","json.loads Failed to convert \""+payload_str+"\" to JSON.",0,True)
            client.publish ("tnz2ha/debug_e",str (e),0,True)
        if (not "Device" in payload_str):
            client.publish ("tnz2ha/debug_eeee","We need to do something different")
        else:
            try:
                # TODO - make this more robust
                for p in payload.items ():
                    process_misc (client,p [1])
            except Exception as e:
                client.publish ("tnz2ha/debug_error","process_misc Failed to process \""+payload_str+"\"."+str (e),0,True)
                client.publish ("tnz2ha/debug_e",str (e),0,True)

if __name__=="__main__":
    #global client

    parser=argparse.ArgumentParser (description='Various options for tnz2ha.')
    parser.add_argument ("--mqtt-topic",help="MQTT Topic")
    parser.add_argument ("--mqtt-disc",help="MQTT Discovery Topic")
    parser.add_argument ("--mqtt-host",help="MQTT Host")
    parser.add_argument ("--mqtt-port",help="MQTT Port",default=1883)
    parser.add_argument ("--mqtt-user",help="MQTT Username")
    parser.add_argument ("--mqtt-pass",help="MQTT Password")
    parser.add_argument ("--tasm-host",help="Tasmota Host")
    parser.add_argument ("--tasm-port",help="Tasmota Port",default=80)
    parser.add_argument ("--tasm-user",help="Tasmota Username")
    parser.add_argument ("--tasm-pass",help="Tasmota Password")
    args=parser.parse_args ()

    mqtt_topic=args.mqtt_topic
    mqtt_disc=args.mqtt_disc
    tasm_topic=""

#    client=mqtt.Client()
    client.on_connect=on_connect
    client.on_message=on_message

    client.connect (args.mqtt_host,int (args.mqtt_port),60)

    old_sec=-1
    old_minu=-1
    old_hour=-1
    uptime=0

    client.publish ("tnz2ha/version",)

    while run:
        now=datetime.datetime.now ()
        sec=now.second
        if (sec!=old_sec):
            uptime=uptime+1
            client.publish ("tnz2ha/uptime",uptime)
            old_sec=sec
            hour=now.hour
            minu=now.minute
            if (minu!=old_minu):
                logging.debug ("Running per minute tasks")
                old_minu=minu
                if (hour!=old_hour):
                    logging.debug ("Running per hour tasks")
                    old_hour=hour
                    PROXIES={}
                    HEADERS={}
                    PARAMS={}
                    tasm_topic=""
                    zbstatus={}
                    zbstatus3={}
                    URL="http://"+args.tasm_host+":"+args.tasm_port+"/cm?cmnd=SetOption60 0"
                    try:
                        r=requests.get (url=URL,proxies=PROXIES,headers=HEADERS,params=PARAMS)
                    except:
                        logging.error ("Failed to Set Option")
                    URL="http://"+args.tasm_host+":"+args.tasm_port+"/cm?cmnd=SetOption83 0"
                    try:
                        r=requests.get (url=URL,proxies=PROXIES,headers=HEADERS,params=PARAMS)
                    except:
                        logging.error ("Failed to Set Option")
                    URL="http://"+args.tasm_host+":"+args.tasm_port+"/cm?cmnd=SetOption89 1"
                    try:
                        r=requests.get (url=URL,proxies=PROXIES,headers=HEADERS,params=PARAMS)
                    except:
                        logging.error ("Failed to Set Option")
                    URL="http://"+args.tasm_host+":"+args.tasm_port+"/cm?cmnd=SetOption100 1"
                    try:
                        r=requests.get (url=URL,proxies=PROXIES,headers=HEADERS,params=PARAMS)
                    except:
                        logging.error ("Failed to Set Option")
                    URL="http://"+args.tasm_host+":"+args.tasm_port+"/cm?cmnd=SetOption101 0"
                    try:
                        r=requests.get (url=URL,proxies=PROXIES,headers=HEADERS,params=PARAMS)
                    except:
                        logging.error ("Failed to Set Option")
                    URL="http://"+args.tasm_host+":"+args.tasm_port+"/cm?cmnd=SetOption110 0"
                    try:
                        r=requests.get (url=URL,proxies=PROXIES,headers=HEADERS,params=PARAMS)
                    except:
                        logging.error ("Failed to Set Option")
                    URL="http://"+args.tasm_host+":"+args.tasm_port+"/cm?cmnd=Status"
                    try:
                        r=requests.get (url=URL,proxies=PROXIES,headers=HEADERS,params=PARAMS)
                        status=json.loads (r.text)
                        tasm_topic=status ["Status"]["Topic"]
                    except:
                        logging.error ("Failed to Set Option")
                    URL="http://"+args.tasm_host+":"+args.tasm_port+"/cm?cmnd=ZbStatus1"
                    try:
                        r=requests.get (url=URL,proxies=PROXIES,headers=HEADERS,params=PARAMS)
                        zbstatus=json.loads (r.text)
                    except:
                        logging.error ("Failed to get status")
                    logging.debug ("Sending devices to MQTT")
                    try:
                        client.publish ("tnz2ha/devices",r.text,0,False)
                    except:
                        logging.error ("Failed to publish devices")
                    if ("ZbStatus1" in zbstatus):
                        logging.debug ("Found ZbStatus1")
                        for dev_value in zbstatus ["ZbStatus1"]:
                            for key,value in dev_value.items ():
                                if key=="Device":
                                    devices [str (value)]={}
                                    URL="http://"+args.tasm_host+":"+args.tasm_port+"/cm?cmnd=ZbStatus3 "+value
                                    try:
                                        r=requests.get (url=URL,proxies=PROXIES,headers=HEADERS,params=PARAMS)
                                        zbstatus3=json.loads (r.text)
                                    except:
                                        logging.error ("Failed to get status")
                                    if ("ZbStatus3" in zbstatus3):
                                        logging.debug ("Found ZbStatus3")
                                        for zbs3_value in zbstatus3 ["ZbStatus3"]:
                                            for zbs3_key,zbs3_val in zbs3_value.items ():
                                                devices [value][zbs3_key]=zbs3_val
                        client.publish ("tnz2ha/loaded_devices",json.dumps (devices),0,False)
        client.loop (0.25)