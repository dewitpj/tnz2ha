# Work-In-Progress !!!

This add-on allows you to add Zigbee devices to Home Assistant, using MQTT Auto Discovery. It will configure the Tasmota module to the settings that it needs.

## Requirements:

* ZbBridge - Currently only tested with the Sonoff Zigbee to WiFi Bridge
* Flash bridge with Tasmota - Tested on 9.3.1.2+
* Set the module to "Sonoff ZbBridge (75)"
* MQTT - Tested with an external MQTT but should work with any MQTT (HA and Tasmota needs to talk to the same MQTT)
* Configure the module to use the MQTT server - will be automated at some time

## Installation

1. Logon to your HA or HASS with SSH
2. Got to the HA `addons` directory within the HA installation path (if this is not available - create this directory).
3. Run `cd addons`
4. Run `git clone https://gitlab.com/dewitpj/tnz2ha.git` within the `addons` directory
5. Reload your local addons from the Add-on Store
6. Configure and enjoy

Questions? Check out the [gitlab project](https://gitlab.com/dewitpj/tnz2ha/-/issues)

## Known Issues:

* Currently, HA doesn't display the battery percentage sensors on the dashboard - [Logged with HA](https://github.com/home-assistant/frontend/issues/8754)
* MQTT Configuration is manual on the Tasmota device (For now)

## TODO:

* Auto discover and track Tasmota devices (so we can cope with changing IPs)

## Guide

The only part that needs configuration is where HA (or something else) sends the MQTT message for changing the values. This is done as follow:

* `Send 25 to "tnz2ha/cmnd/<short id>/1/dimmer_cmd"`

This will set endpoint 1 to 25% (not 25 - the value is calculated - `<value>*2.55`)

* `Send ON to "tnz2ha/cmnd/<short id>/A/state_cmd"`

This will turn on all endpoints - this also works with the dimmer_cmd
